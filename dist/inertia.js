;(function($){
    "use strict";
    $.fn.inertia = function(controller) {

        var $this = this,
            options = $this.data("inertia"),
            previousPosition = 0;

        options && (controller = options) || (controller = {});
        controller.frame || (controller.frame = ".inertia");
        controller.weight || (controller.weight = 2);
        controller.duration || (controller.duration = 0.7);
        controller.ease || (controller.ease = Power3.easeOut);

        $(window).on("resize scroll", function(){
            var offset = $this.closest($(controller.frame)).offset().top,
                currentPosition = $(window).scrollTop(),
                ratio = (currentPosition - offset)/$(window).height(),
                y = - controller.weight * ratio * 100,
                autoAlpha = (1 - ratio * 1);
            // console.log(controller.alpha);    
            // (controller.alpha == 1) || (controller.alpha = autoAlpha);
            // if(controller.alpha == "ture"){
            //     controller.alpha = autoAlpha;
            // }
            // console.log(controller.alpha);                
            (currentPosition = previousPosition) || TweenMax.to($this, controller.duration, {opacity: autoAlpha, y: y, ease: controller.ease});

            previousPosition = currentPosition;
        });

    }
}(jQuery));